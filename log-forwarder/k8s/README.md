# Sidecar adapter container: Log forwarder demo

This is a log forwarder demo: the delivered app uses file logs instead of stdout.

The goal is to show the usage of a dedicated sidecar container reading the log file content and writing it to stdout as json thanks to fluent-bit

- create the deployment from `01-deploy-app.yaml` and try to check the logs with `kubectl logs`
- try a `kubectl exec` to check where the logs are written
- apply the deployment from `02-deploy-debug.yaml` and if the logs appear in /logs
- apply the manifests from `03a-fluent-bit-config-basic.yaml`, `03b-pvc.yaml` and `03c-deploy-log-forwarder.yaml` and check the logs from log-forwarder container
- apply the new config from `04-fluent-bit-config-apache-parsing.yaml` and delete the current pod, check the resulting logs from log-forwarder container once parsed

