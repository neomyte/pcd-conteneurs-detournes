# IDE

To build: `docker build -t ide --build-arg UID=$(id -u) .`

To run: `docker run -it --rm -v /tmp/.X11-unix:/tmp/.X11-unix -v$(pwd):/app ide`
