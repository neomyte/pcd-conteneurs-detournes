# nsenter demo

`nsenter` (part of util-linux) allows you to access the underlying host of a container provided you execute it with sufficient privileges.

The script `nsenter-node.sh` starts a container scheduled on a requested host with the host PID namespace.

As a result, the nsenter command is able to target the host namespaces to give a shell on the host.

```
minikube start
./nsenter-node.sh minikube
```

## Notes

Using version 2.34 is recommended if your kernel does not support the time namespace.

## References

https://github.com/alexei-led/nsenter
https://hub.docker.com/r/alexeiled/nsenter
