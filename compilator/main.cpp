#include<iostream>

int main(int argc, char ** argv){
    using std::cout;
    using std::cerr;

    if(argc > 1){
        cerr << "Argument were given but none were exepected, the following will be ignored:\n";
        for(int i{1}; i < argc; ++i){
            cerr << " * " << argv[i] << '\n';
        }
    }

    cout << "Hello, World !\n";
}
