# Builder

To build the compilator: `docker build -t compilator .`

To run the compilator: `docker run -it --rm -v$(pwd):/app compilator`

To run the compilator with std11: `docker run -it --rm -v$(pwd):/app compilator g++ main.cpp -o hello_world -static -std=c++11`

To run the application: `./hello_world`
