# Builder

To build the application: `docker build -t builder .`

To run the application: `docker run -it --rm builder`
